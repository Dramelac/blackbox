#------import------
import pygame, sys, random, pickle, os
from pygame.locals import *
from Interface import *
from reseau import *


def init():
    #-------Constante Init-----
    place_atom = 0
    Number = 0
    score = 0
    PlayGame = True
    button = False
    #--------Fonction---------
    return place_atom, Number, score, PlayGame, button

def Init_grille(x_grille):
    #init grille (x_grille)
    grille = [['.']*x_grille for i in range(x_grille)]
    return grille

def Rdm_Atom(Cb_atom, x_grille):
    #random placement des cb_atom
    Hide_Atom = [[0]*2 for i in range(Cb_atom)]
    HideGrille = [["."]*x_grille for i in range(x_grille)]
    id1 = 0
    while id1 < Cb_atom:
        a = random.randrange(1,x_grille-1)
        b = random.randrange(1,x_grille-1)
        Hide_Atom[id1] = [a, b]

        id1, HideGrille = check_rdm(Hide_Atom, id1, HideGrille)

    return Hide_Atom, HideGrille

def check_rdm(Hide_atom, id1, HideGrille):
    for check in range(0,id1):
            if Hide_atom[check] == Hide_atom[id1]:
                return id1, HideGrille
    HideGrille[Hide_atom[id1][0]][Hide_atom[id1][1]] = 'X'
    return id1+1, HideGrille

def CordLaser(w, z, Va, Vb):
    i0 = w + Va + Vb
    j0 = z + Va + Vb
    i1 = i0 - 1 * Vb
    j1 = j0 - 1 * Va
    i2 = i0 - 2 * Vb
    j2 = j0 - 2 * Va

    return i0, i1, i2, j0, j1, j2

def laser(w, z, Va, Vb, HideGrille, x_grille):
    a, b = w, z

    i0, i1, i2, j0, j1, j2 = CordLaser(w, y, Va, Vb)

    if HideGrille[i1][j1] == 'X':
        return 0, 0 #return Rouge

    if HideGrille[i0][j0] == 'X' or HideGrille[i2][j2] == 'X':
        return a, b #return Jaune

    while True:
        w += Va
        z += Vb

        if z <= 0 or w <= 0 or z > x_grille or w > x_grille:
            return w, z

        i0, i1, i2, j0, j1, j2 = CordLaser(w, z, Va, Vb)

        try:

            if HideGrille[i1][j1] == 'X':
                return 0, 0  #return Rouge

            if HideGrille[i0][j0] == 'X' and HideGrille[i2][j2] == 'X':
                #demi tour
                return a, b

            if HideGrille[i0][j0] == 'X':
                #changment de direction
                temp = (-1 * Vb)
                Vb = (-1 * Va)
                Va = temp

            if HideGrille[i2][j2] == 'X':
                #changement de direction
                temp = Vb
                Vb = Va
                Va = temp

        except IndexError:
            return w, z

    return w, z

def afficherResult(a, b, x, y, Number):
    font = pygame.font.Font(None, 30)
    if a == 0 and b == 0:
        pygame.draw.rect(maSurface, RED, BOARDRECTS[x][y], 1)
    elif a == x and b == y:
        pygame.draw.rect(maSurface, YELLOW, BOARDRECTS[x][y], 1)
    else:
        numero = font.render(str(Number), 1, WHITE)
        maSurface.blit(case2, BOARDRECTS[x][y])
        maSurface.blit(numero, (BOARDRECTS[x][y][0]+15, BOARDRECTS[x][y][1]+10))
        maSurface.blit(case2, BOARDRECTS[a][b])
        maSurface.blit(numero, (BOARDRECTS[a][b][0]+15, BOARDRECTS[a][b][1]+10))
        #blit number on a b and x y
        Number += 1
    pygame.display.update()
    return Number

def getClick(x_grille):
    Va, Vb = 0, 0
    temp = 0
    for x in range(0, x_grille):
        for y in range(0, x_grille):
            if BOARDRECTS[x][y].collidepoint(event.pos):
                if x == 0:
                    Va = 1
                    temp += 1
                if x == x_grille-1:
                    Va = -1
                    temp += 1
                if y == 0:
                    Vb = 1
                    temp += 1
                if y == x_grille-1:
                    Vb = -1
                    temp += 1
                if temp > 1:
                    return x, y, 0, 0
                return x, y, Va, Vb
    return 0, 0, 0, 0

def placeAtom(x, y, grille, place_atom, Atom,Cb_atom, score=0):
    if grille[x][y] == "X": # retirer atome
        grille[x][y] = "."
        maSurface.blit(case, BOARDRECTS[x][y])
        for i in range(place_atom):# decaler liste
            if Atom[i] == [x, y]:
                try:
                    for y in range(i, Cb_atom):
                        Atom[y] = Atom[y+1]
                    break
                except IndexError:
                    Atom[y] = [0, 0]
                    break
        place_atom -= 1
    elif place_atom < Cb_atom: #placer les atomes s'il en reste
        maSurface.blit(atome_orange_img, BOARDRECTS[x][y])
        grille[x][y] = "X"
        Atom[place_atom] = [x, y]
        place_atom += 1

    fontObj = pygame.font.Font("data/blackbox_font.ttf", 50)
    if place_atom == Cb_atom:
        maSurface.blit(buttonGreen,buttonGreen_rect)
        valide = fontObj.render('Valider', True, WHITE)
        valideRect = valide.get_rect()
        valideRect.topleft = (LARGEUR-200, HAUTEUR-450)
        maSurface.blit(valide, valideRect)
    else:
        maSurface.blit(buttonGrey,buttonGrey_rect)
        valide = fontObj.render('Valider', True, WHITE)
        valideRect = valide.get_rect()
        valideRect.topleft = (LARGEUR-200, HAUTEUR-450)
        maSurface.blit(valide, valideRect)

    pygame.display.update()
    return grille, place_atom, Atom, score

def init_scoreboard(): #chargement du scoreboard
    try:
        mon_fichier = open("data/scoreboard", 'rb')

        mon_depickler = pickle.Unpickler(mon_fichier)
        liste_score = mon_depickler.load()

        mon_fichier.close()

    except IOError:
        liste_score = [['-', 999] for i in range(30)]

    return liste_score

def write_scoreboard(liste_score):   #enregistrement du scoreboard dans le fichier

    fichier = open("data/scoreboard", "wb")
    mon_pickler = pickle.Pickler(fichier)
    mon_pickler.dump(liste_score)

    fichier.close()

def place_scoreboard(liste_score, score, pseudo, Cb_atom): #trie des donner dans la liste

    if Cb_atom == 3:
        if liste_score[9][1] > score:
            liste_score[9][1] = score
            liste_score[9][0] = pseudo

            for i in range(9, 0, -1):

                if liste_score[i][1] < liste_score[i - 1][1]:

                    for j in range(2):
                        temp = liste_score[i][j]
                        liste_score[i][j] = liste_score[i - 1][j]
                        liste_score[i - 1][j] = temp

                else:
                    write_scoreboard(liste_score)
                    return 1

            write_scoreboard(liste_score)

        else:
            return 0

    elif Cb_atom == 4:
        if liste_score[19][1] > score:
            liste_score[19][1] = score
            liste_score[19][0] = pseudo

            for i in range(19, 10, -1):

                if liste_score[i][1] < liste_score[i - 1][1]:

                    for j in range(2):
                        temp = liste_score[i][j]
                        liste_score[i][j] = liste_score[i - 1][j]
                        liste_score[i - 1][j] = temp

                else:
                    write_scoreboard(liste_score)
                    return 1

            write_scoreboard(liste_score)

        else:
            return 0

    elif Cb_atom == 5:
        if liste_score[29][1] > score:
            liste_score[29][1] = score
            liste_score[29][0] = pseudo

            for i in range(29, 20, -1):

                if liste_score[i][1] < liste_score[i - 1][1]:

                    for j in range(2):
                        temp = liste_score[i][j]
                        liste_score[i][j] = liste_score[i - 1][j]
                        liste_score[i - 1][j] = temp

                else:
                    write_scoreboard(liste_score)
                    return 1

            write_scoreboard(liste_score)

        else:
            return 0

def affichage_pseudo(pseudo, pseudo2=""):

    font = pygame.font.Font(None, 30)
    pseudoImg = font.render(pseudo, 1, (255, 255, 255))
    pseudo2Img = font.render(pseudo2, 1, (255, 255, 255))

    pseudoImg_rect = pseudoImg.get_rect(center=(LARGEUR/10, HAUTEUR/10))

    pseudo2Img_rect = pseudo2Img.get_rect(center=(LARGEUR/10, HAUTEUR*0.9))

    maSurface.blit(pseudoImg, pseudoImg_rect)
    maSurface.blit(pseudo2Img, pseudo2Img_rect)

#----------------------Reseau--------------------------#

def placerAtomesParJoueur(Cb_atom, x_grille):

    #placement des atomes par le joueur
    Hide_Atom = [[0]*2 for i in range(Cb_atom)]
    HideGrille = [["."]*x_grille for i in range(x_grille)]
    id1 = 0
    while id1 < Cb_atom:
        for event in pygame.event.get():
            if event.type == MOUSEBUTTONDOWN:
                x, y = getClick(x_grille) # recuperer le clic du joueur
                HideGrille, place_atom, Hide_Atom = place_atom(x, y, HideGrille, place_atom, Hide_Atom,Cb_atom) #placer les atomes dans la grille

    return Hide_Atom, HideGrille


#---------Main-----------


pseudo = start_1()
liste_score = init_scoreboard()

while True:
    place_atom, Number, score, PlayGame, button = init()
    Cb_atom, x_grille = start_2(liste_score)

    Atom = [[0, 0]for i in range(Cb_atom)]

    grille = Init_grille(x_grille)

    if start_5():
        print("Demarrage du server")
        Hide_Atom, HideGrille = Rdm_Atom(Cb_atom, x_grille) # retourne une grille avec atomes places
        player_gamer = 2
        #synchroJoueursInterface("En attente de joueur ...")
        player_gamer = 2
        synchroJoueursInterface("Démarrage du serveur local ...")

        connexionPrincipale, connexionClient = CreationServeur()

        synchroJoueursInterface("Connection en cours ...")

        EnvoieDifficult(connexionClient, x_grille, Cb_atom, pseudo)

        Hide_Atom, HideGrille = Rdm_Atom(Cb_atom, x_grille)
        #demarrer les procedure server
        pseudo2 = "Client"
        affichage_pseudo(pseudo, pseudo2)
    else:
        print("Demmarage du jeu local : random")
        Hide_Atom, HideGrille = Rdm_Atom(Cb_atom, x_grille)
        player_gamer = 1

        affichage_pseudo(pseudo)
    BOARDRECTS = init_grille(x_grille)
    actu_grille(x_grille, BOARDRECTS)

    valideRect = validerScreen(place_atom,Cb_atom)

    while PlayGame: #true -> event submit

        for event in pygame.event.get():
            quit(event)

            #lancement du laser + calcul trajectoire

            if event.type == MOUSEBUTTONDOWN:
                x, y, Va, Vb = getClick(x_grille)

                if not button and Va == 0 and Vb == 0 and x != 0 and x != x_grille-1 and y != x_grille-1:
                    grille, place_atom, Atom, score = placeAtom(x, y, grille, place_atom, Atom, Cb_atom, score)
                elif not button and Va != 0 or Vb != 0:
                    score += 1
                    a, b = laser(x, y, Va, Vb, HideGrille, x_grille)
                    Number = afficherResult(a, b, x, y, Number)
                if button and recommencerRect.collidepoint(event.pos) and place_atom == Cb_atom:
                    PlayGame = False
                if valideRect.collidepoint(event.pos) and place_atom == Cb_atom and not button:
                    score = solutionTest(Cb_atom, Hide_Atom, Atom, BOARDRECTS, score)
                    buttonOrange_rect = buttonOrange.get_rect(center=(LARGEUR-130, HAUTEUR-415))
                    recommencerRect = recommencerScreen()
                    button = True

                    place_scoreboard(liste_score, score, pseudo, Cb_atom)


            #affichage d'emission / reception + caractère spécial

        updateScore(score)
        fpsClock.tick(FPS)

    #correction()
    #reset()
