#-------------------------------------------Init Pygame------------------------------------------#

import pygame, sys, pickle
from pygame.locals import *
from reseau import *

WHITE = (255, 255, 255)
RED = (255, 0, 0)
YELLOW = (150, 150, 0)

LARGEUR = 1000
HAUTEUR = 600

FPS = 60

pygame.init()

fpsClock = pygame.time.Clock()

maSurface = pygame.display.set_mode((LARGEUR, HAUTEUR))
pygame.display.set_caption('BlackBox')

#-------------------------------------------Import Images-----------------------------------------#

start_B = pygame.image.load("data/Start_B.png")  # Start button
start_B_rect = start_B.get_rect(center=(LARGEUR/2, HAUTEUR/1.5))

background = pygame.image.load("data/background.png")  # Game background

credit = pygame.image.load("data/credits.png")  # Credits button
credits_rect = credit.get_rect(center=(LARGEUR/2, HAUTEUR/1.5))

Play = pygame.image.load("data/Play.png")  # Play button
Play_rect = Play.get_rect(center=(LARGEUR/2, HAUTEUR/3))

LanImg = pygame.image.load("data/lan.png")  # Lan button
LanImg_rect = LanImg.get_rect(center=(LARGEUR-200, HAUTEUR/3))

player = pygame.image.load("data/player.png")  # 1 player mode
player_rect = player.get_rect(center=(LARGEUR/2, HAUTEUR/2.5))

players2 = pygame.image.load("data/players2.png")  # 2 players mode
players2_rect = players2.get_rect(center=(LARGEUR/2, HAUTEUR/1.7))

quitter = pygame.image.load("data/quitter.png")  # Quit button
quitter_rect = quitter.get_rect(center=(LARGEUR/2, HAUTEUR/1.2))

tutoriel = pygame.image.load("data/tutoriel.png")  # Tutorial button
tutoriel_rect = tutoriel.get_rect(center=(LARGEUR/2, HAUTEUR/2))

d_easy = pygame.image.load("data/easy.png")  # Easy mode
easy_rect = d_easy.get_rect(center=(LARGEUR/2, HAUTEUR/2.5))

d_normal = pygame.image.load("data/normal.png")  # Normal mode
normal_rect = d_normal.get_rect(center=(LARGEUR/2, HAUTEUR/2 + 50))

d_difficult = pygame.image.load("data/difficult.png")  # Difficult mode
difficult_rect = d_difficult.get_rect(center=(LARGEUR/2, HAUTEUR/2 + 160))

d_grille = pygame.image.load("data/xgrille.png")  # Enter board size !! To be modified (rectangle)
grille_rect = d_grille.get_rect(center=(LARGEUR/2,HAUTEUR/3))

case = pygame.image.load("data/case.png")  # Case picture
case_rect = case.get_rect(center=(LARGEUR/3, HAUTEUR/3))

case2 = pygame.image.load("data/case2.png")  # Laser case picture
case2_rect = case2.get_rect(center=(LARGEUR/3, HAUTEUR/3))

buttonOrange = pygame.image.load("data/buttonOrange.png")
buttonOrange_rect = buttonOrange.get_rect(center=(LARGEUR-130, HAUTEUR-415))

buttonGrey = pygame.image.load("data/buttonGrey.png")
buttonGrey_rect = buttonGrey.get_rect(center=(LARGEUR-130, HAUTEUR-415))

buttonGreen = pygame.image.load("data/buttonGreen.png")
buttonGreen_rect = buttonGreen.get_rect(center=(LARGEUR-130, HAUTEUR-415))

scoreboard_img = pygame.image.load("data/scoreboard_button.png")
scoreboard_rect = scoreboard_img.get_rect(center=(LARGEUR-860, HAUTEUR-120))

retour_scoreboard = pygame.image.load("data/retour_scoreboard.png")
retour_scoreboard_rect = retour_scoreboard.get_rect(center=(LARGEUR-890, HAUTEUR-520))

blackbox_menu = pygame.image.load("data/game_bg.png")  # Game background

cache_score = pygame.image.load("data/score_cache.png")
atome_green_img = pygame.image.load("data/atome_green.png")
atome_red_img = pygame.image.load("data/atome_red.png")
atome_orange_img = pygame.image.load("data/atome_orange.png")

#------------------------------------------------------------------------------------------------#

def quit(event):
    if event.type == QUIT:
        pygame.quit()
        sys.exit()
    #croix rouge

def credits():
    maSurface.blit(background, (0, 0))
    fontObj = pygame.font.Font("data/blackbox_font.ttf", 43)
    Mathieu = fontObj.render('Calemard Du Gardin Mathieu', True, WHITE)
    Daria = fontObj.render('Kobtseva Daria', True, WHITE)
    David = fontObj.render('Robert David', True, WHITE)
    Antoine = fontObj.render('Simonin Antoine', True, WHITE)
    Corentin = fontObj.render('Vatan Corentin', True, WHITE)
    Crew = fontObj.render("Supinfo ASC1 projet de fin d'année 2014/2015 ©", True, WHITE)

    Mathieu_rect = Mathieu.get_rect(center=(LARGEUR/2, 200))
    Daria_rect = Daria.get_rect(center=(LARGEUR/2, 250))
    David_rect = David.get_rect(center=(LARGEUR/2, 300))
    Antoine_rect = Antoine.get_rect(center=(LARGEUR/2, 350))
    Corentin_rect = Corentin.get_rect(center=(LARGEUR/2, 400))
    Crew_rect = Crew.get_rect(center=(LARGEUR/2, 500))

    maSurface.blit(Mathieu, Mathieu_rect)
    maSurface.blit(Daria, Daria_rect)
    maSurface.blit(David, David_rect)
    maSurface.blit(Antoine, Antoine_rect)
    maSurface.blit(Corentin, Corentin_rect)
    maSurface.blit(Crew, Crew_rect)
    maSurface.blit(retour_scoreboard, retour_scoreboard_rect)
    pygame.display.update()

    while True:
        for event in pygame.event.get():
            quit(event)
            if event.type == MOUSEBUTTONDOWN and retour_scoreboard_rect.collidepoint(event.pos):
                maSurface.blit(background, (0, 0))
                maSurface.blit(Play, Play_rect)
                maSurface.blit(tutoriel, tutoriel_rect)
                maSurface.blit(credit, credits_rect)
                maSurface.blit(quitter, quitter_rect)
                maSurface.blit(scoreboard_img, scoreboard_rect)
                pygame.display.update()
                return
        fpsClock.tick(FPS)

def displayScoreboard(difficulty, liste_score):
    font = pygame.font.Font("data/blackbox_font.ttf", 30)
    for i in range(10):
        texteSurface = font.render(str(liste_score[difficulty * 10 + i][0]) + " : " + str(liste_score[difficulty * 10 + i][1]), True, WHITE)
        texteRect = texteSurface.get_rect(center=(LARGEUR/2, HAUTEUR/16 * i + 150))
        maSurface.blit(texteSurface, texteRect)

def leaderboard_menu(liste_score):

    mon_fichier = open("data/scoreboard", 'rb')

    mon_depickler = pickle.Unpickler(mon_fichier)
    liste_score = mon_depickler.load()

    mon_fichier.close()

    Select = pygame.image.load("data/Select.png")
    easySelectRect = Select.get_rect(center=(LARGEUR/3 - 150, HAUTEUR-50))
    normalSelectRect = Select.get_rect(center=(2*LARGEUR/3 - 150, HAUTEUR-50))
    difficultSelectRect = Select.get_rect(center=(LARGEUR - 150, HAUTEUR-50))

    maSurface.blit(background, (0, 0))
    maSurface.blit(Select, (easySelectRect[0]-9, easySelectRect[1]-2))
    maSurface.blit(d_easy, easySelectRect)
    maSurface.blit(d_normal, normalSelectRect)
    maSurface.blit(d_difficult, difficultSelectRect)
    maSurface.blit(retour_scoreboard, retour_scoreboard_rect)

    displayScoreboard(0, liste_score)

    pygame.display.update()


    while True:

        for event in pygame.event.get():
            quit(event)
            if event.type == MOUSEBUTTONDOWN:
                maSurface.blit(background, (0, 0))
                if easySelectRect.collidepoint(event.pos):
                    maSurface.blit(Select, (easySelectRect[0]-9, easySelectRect[1]-2))
                    displayScoreboard(0, liste_score)

                elif normalSelectRect.collidepoint(event.pos):
                    maSurface.blit(Select, (normalSelectRect[0]-9, normalSelectRect[1]-2))
                    displayScoreboard(1, liste_score)

                elif difficultSelectRect.collidepoint(event.pos):
                    maSurface.blit(Select, (difficultSelectRect[0]-9, difficultSelectRect[1]-2))
                    displayScoreboard(2, liste_score)

                elif retour_scoreboard_rect.collidepoint(event.pos):
                    maSurface.blit(background, (0, 0))
                    maSurface.blit(Play, Play_rect)
                    maSurface.blit(tutoriel, tutoriel_rect)
                    maSurface.blit(credit, credits_rect)
                    maSurface.blit(quitter, quitter_rect)
                    maSurface.blit(scoreboard_img, scoreboard_rect)
                    pygame.display.update()
                    return

                else:
                    break
                maSurface.blit(d_easy, easySelectRect)
                maSurface.blit(d_normal, normalSelectRect)
                maSurface.blit(d_difficult, difficultSelectRect)
                maSurface.blit(retour_scoreboard, retour_scoreboard_rect)

                pygame.display.update()
        fpsClock.tick(FPS)

def start_1():

    print("Veuillez entrer votre pseudo")
    name = ""
    font = pygame.font.Font("data/blackbox_font.ttf", 50)

    pygame.display.update()

    while True:

        for event in pygame.event.get(): # event handling loop

            quit(event)

            if event.type == KEYDOWN:
                if 32 < event.key < 123: # input name
                    name += event.unicode
                    if len(name) >= 12:
                        name = name[:-1]
                elif event.key == K_BACKSPACE:
                    name = name[:-1]
                elif event.key == K_RETURN and name != "":
                    return name

            elif event.type == MOUSEBUTTONDOWN and start_B_rect.left < event.pos[0] < start_B_rect.right and start_B_rect.top < event.pos[1] < start_B_rect.bottom and name != "":
                return name

        maSurface.blit(background, (0, 0))
        maSurface.blit(start_B, start_B_rect)
        #d_pseudo = pygame.draw.rect(maSurface, (0, 0, 0), (350, 175, 300, 50), 0)
        d_pseudo = pygame.Surface((300, 50))  # Create new surface
        d_pseudo.set_alpha(128)  # Set alpha level
        d_pseudo.fill((255, 255, 255))
        maSurface.blit(d_pseudo, (350,175))
        name_block = font.render(name, True, (255, 255, 255))
        name_block_rect = name_block.get_rect(center=(LARGEUR/2, HAUTEUR/3))
        maSurface.blit(name_block, name_block_rect)
        pygame.display.flip()

        fpsClock.tick(FPS)

def start_2(liste_score):

    maSurface.blit(background, (0, 0))
    maSurface.blit(Play, Play_rect)
    maSurface.blit(LanImg, LanImg_rect)
    maSurface.blit(tutoriel, tutoriel_rect)
    maSurface.blit(credit, credits_rect)
    maSurface.blit(quitter, quitter_rect)
    maSurface.blit(scoreboard_img, scoreboard_rect)
    pygame.display.update()

    while True:

        for event in pygame.event.get():
            quit(event)

            if event.type == MOUSEBUTTONDOWN and Play_rect.collidepoint(event.pos):
                Cb_atom = start_3()
                x_grille = start_4() + 2
                return Cb_atom, x_grille

            if event.type == MOUSEBUTTONDOWN and LanImg_rect.collidepoint(event.pos):
                synchroJoueursInterface("Connection en cours ...")
                connexionServeur = CreationClient("localhost")
                synchroJoueursInterface("Synchronisation du client ...")
                x_grille, Cb_atom, pseudo2 = ReceptDiff(connexionServeur)
                print(x_grille, Cb_atom, pseudo2)

            if event.type == MOUSEBUTTONDOWN and tutoriel_rect.collidepoint(event.pos):
                print("Work in progress")

            if event.type == MOUSEBUTTONDOWN and credits_rect.collidepoint(event.pos):
                credits()

            if event.type == MOUSEBUTTONDOWN and quitter_rect.collidepoint(event.pos):
                pygame.quit()
                sys.exit()

            if event.type == MOUSEBUTTONDOWN and scoreboard_rect.collidepoint(event.pos):
                leaderboard_menu(liste_score)
        fpsClock.tick(FPS)

def start_3():

    maSurface.blit(background, (0, 0))
    maSurface.blit(d_easy, easy_rect)
    maSurface.blit(d_normal, normal_rect)
    maSurface.blit(d_difficult, difficult_rect)
    pygame.display.update()

    while True:
        for event in pygame.event.get(): # event handling loop

            quit(event)

            if event.type == MOUSEBUTTONDOWN:
                if easy_rect.left < event.pos[0] < easy_rect.right and easy_rect.top < event.pos[1] < easy_rect.bottom:
                    return 3
                if normal_rect.left < event.pos[0] < normal_rect.right and normal_rect.top < event.pos[1] < normal_rect.bottom:
                    return 4
                if difficult_rect.left < event.pos[0] < difficult_rect.right and difficult_rect.top < event.pos[1] < difficult_rect.bottom:
                    return 5

        fpsClock.tick(FPS)

def start_4():

    x_grille = '8'
    font = pygame.font.Font(None, 50)

    pygame.display.update()

    while True:
        for event in pygame.event.get(): # event handling loop

            quit(event)

            if event.type == KEYDOWN:
                if 47 < event.key < 58: # Only numbers input
                    x_grille += event.unicode
                    if len(x_grille) >= 3:
                        x_grille = x_grille[:-1]
                elif event.key == K_BACKSPACE:
                    x_grille = x_grille[:-1]
                elif event.key == K_RETURN and 8 <= int(x_grille) <= 12:
                    return int(x_grille)
                elif event.key == K_RETURN and int(x_grille) < 8:
                    x_grille = 8
                    print(x_grille)
                    return x_grille
                elif event.key == K_RETURN and int(x_grille) > 9:
                    x_grille = 9
                    print(x_grille)
                    return x_grille

            elif event.type == MOUSEBUTTONDOWN and grille_rect.left < event.pos[0] < grille_rect.right and grille_rect.top < event.pos[1] < grille_rect.bottom:
                return int(x_grille)

        pygame.display.flip()

        maSurface.blit(background, (0, 0))
        # d_grille = pygame.draw.rect(maSurface, (0, 0, 0), (350, 175, 300, 50), 0)
        d_grille = pygame.Surface((100, 50))  # Create new surface
        d_grille.set_alpha(128)  # Set alpha level
        d_grille.fill((255, 165, 0))
        maSurface.blit(d_grille, (450, 175))
        x_grille_block = font.render(str(x_grille), True, (255, 255, 255))
        x_grille_block_rect = x_grille_block.get_rect(center=(LARGEUR/2, HAUTEUR/3))
        maSurface.blit(x_grille_block, x_grille_block_rect)

        pygame.display.flip()

        fpsClock.tick(FPS)

def start_5():

    maSurface.blit(background, (0, 0))
    maSurface.blit(player, player_rect)
    maSurface.blit(players2, players2_rect)

    pygame.display.update()

    while True:
        for event in pygame.event.get():

            quit(event)

            if event.type == MOUSEBUTTONDOWN:
                if player_rect.left < event.pos[0] < player_rect.right and player_rect.top < event.pos[1] < player_rect.bottom:
                    return False
                if players2_rect.left < event.pos[0] < players2_rect.right and players2_rect.top < event.pos[1] < players2_rect.bottom:
                    return True
        fpsClock.tick(FPS)

def init_grille(x_grille):
    # Create pygame.Rect objects for each board space to
    # do board-coordinate-to-pixel-coordinate conversions.
    BOARDRECTS = []
    for x in range(x_grille):
        BOARDRECTS.append([])
        for y in range(x_grille):
            XMARGIN = int((LARGEUR - 40 * x_grille) / 2)
            YMARGIN = int((HAUTEUR - 40 * x_grille) / 2)
            r = pygame.Rect((XMARGIN + (x * 40), YMARGIN + (y * 40), 40, 40))
            BOARDRECTS[x].append(r)
    return BOARDRECTS

def actu_grille(x_grille, BOARDRECTS):
    maSurface.blit(blackbox_menu, (0, 0))

    for y in range(1, x_grille-1):

        pygame.draw.rect(maSurface, WHITE, BOARDRECTS[0][y], 1)
        maSurface.blit(case2, BOARDRECTS[0][y])

        pygame.draw.rect(maSurface, WHITE, BOARDRECTS[x_grille-1][y], 1)
        maSurface.blit(case2, BOARDRECTS[x_grille-1][y])

    for x in range(1, x_grille-1):

        pygame.draw.rect(maSurface, WHITE, BOARDRECTS[x][0], 1)
        maSurface.blit(case2, BOARDRECTS[x][0])

        for y in range(1, x_grille-1):
            maSurface.blit(case, BOARDRECTS[x][y])

        pygame.draw.rect(maSurface, WHITE, BOARDRECTS[x][x_grille-1], 1)
        maSurface.blit(case2, BOARDRECTS[x][x_grille-1])

    pygame.display.update()

def solutionTest(Cb_atom, Hide_Atom, Atom, BOARDRECTS, score):
    fail = True
    for dicoA in range(Cb_atom):
        fail = True
        for dicoB in range(Cb_atom):
            #if Hide_Atom[dicoA][0] == Atom[dicoB][0] and Hide_Atom[dicoA][1] == Atom[dicoB][1]:
            if Hide_Atom[dicoA] == Atom[dicoB]:
                maSurface.blit(atome_green_img, BOARDRECTS[Hide_Atom[dicoA][0]][Hide_Atom[dicoA][1]])
                fail = False
                break
                #trouvé !
        if fail:
            maSurface.blit(atome_red_img, BOARDRECTS[Hide_Atom[dicoA][0]][Hide_Atom[dicoA][1]])
            score += 5
        #oublier
    pygame.display.update()
    return score

def updateScore(score):
    font = pygame.font.Font("data/blackbox_font.ttf", 50)
    maSurface.blit(cache_score, (LARGEUR-380, 0))
    score_text = font.render("Score : " + str(score), True, WHITE)
    score_rect = score_text.get_rect(center=(LARGEUR-175, 25))
    maSurface.blit(score_text, score_rect)
    pygame.display.update()

def validerScreen(place_atom,Cb_atom):
    fontObj = pygame.font.Font("data/blackbox_font.ttf", 50)
    maSurface.blit(buttonGrey,buttonGrey_rect)
    valide = fontObj.render('Valider', True, WHITE)
    valideRect = buttonGrey.get_rect()
    valideRect.topleft = (LARGEUR-200, HAUTEUR-450)
    maSurface.blit(valide, valideRect)


    return valideRect

def recommencerScreen():
    fontObj = pygame.font.Font("data/blackbox_font.ttf", 30)
    maSurface.blit(buttonOrange, buttonOrange_rect)
    recommencer = fontObj.render('Recommencer', True, WHITE)
    recommencerRect = buttonOrange.get_rect()
    recommencerRect.topleft = (LARGEUR-215, HAUTEUR-440)
    maSurface.blit(recommencer, recommencerRect)

    return recommencerRect


#----------------------Reseau interface------------------------------#

def synchroJoueursInterface(texte):
    maSurface.blit(background, (0, 0))

    font = pygame.font.Font(None, 50)
    message = font.render(texte, 1, (255, 255, 255))
    message_rect = message.get_rect(center=(LARGEUR/2, HAUTEUR/2))

    maSurface.blit(message, message_rect)

    pygame.display.update()
