import pygame, sys
from pygame.locals import *

import Interface
import socket
#------------------Interface-----------------------------#






#----------------Protocole Reseau -----------------------#

#----------------Serveur----------------#

def CreationServeur():

    hote = ''		#Je suis l'hote
    port = 10800

    connexionPrincipale = socket.socket(socket.AF_INET, socket.SOCK_STREAM)	#TCP (sock stream)
    connexionPrincipale.bind((hote, port))	#socket près a écouter
    connexionPrincipale.listen(1)	#Le socket écoute
    print("Le serveur écoute sur le port", port)
    Interface.synchroJoueursInterface("En attente de joueur ...")
    connexionClient, infosConnexion = connexionPrincipale.accept()
    print("Client connecté :",infosConnexion)
    return connexionPrincipale, connexionClient

def EnvoieDifficult (connexionClient, x_grille, Cb_atom, pseudo) :

    x_grille = bin(x_grille)
    x_grille = x_grille.encode()
    connexionClient.send(x_grille)

    msg = connexionClient.recv(1024)
    print(msg)

    Cb_atom = bin(Cb_atom)
    Cb_atom = Cb_atom.encode()
    connexionClient.send(Cb_atom)

    pseudo2 = connexionClient.recv(1024)
    pseudo2 = pseudo2.decode()
    print(pseudo2)

    pseudo = pseudo.encode()
    connexionClient.send(pseudo)


#----------------Client----------------#


def CreationClient(hote):

    port = 10800
    connexionServeur = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    #TCP (sock stream)
    connexionServeur.connect((hote, port))
    print("Connexion établie avec le serveur sur le port ",port)
    return connexionServeur

def ReceptDiff (pseudo, connexionServeur) :

    x_grille = connexionServeur.recv(1024)
    x_grille = x_grille.decode()
    x_grille = int(x_grille, 2)
    print(x_grille)

    connexionServeur.send(b"5/5")

    Cb_atom = connexionServeur.recv(1024)
    Cb_atom = Cb_atom.decode()
    print(Cb_atom)
    Cb_atom = int(Cb_atom, 2)
    print(Cb_atom)

    pseudo = pseudo.encode()
    connexionServeur.send(pseudo)

    pseudo2 = connexionServeur.recv(1024)
    pseudo2 = pseudo2.decode()
    print(pseudo2)
    return x_grille, Cb_atom, pseudo2



