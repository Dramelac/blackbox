﻿

Black Box
Ou comment créer un jeu vidéo en 10 lignes en mode ninja…

A Supinvaders Project 
Featuring some sleepless people Chapter 1: The ragequit
Magiiic Magiiic !


1. Le jeu 

La ‘blackbox’ est une grille carrée (de n*n cases) où un nombre d’atomes fixe est placé 
Il se joue soit contre un autre joueur soit contre un ordinateur. Le second jouer doit alors deviner où sont les atomes sans pour autant pouvoir voir la grille.

Il doit utiliser les rayons et leur comportement pour deviner la position des atomes. Pour faire cela, il doit les envoyer à partir de chaque côté de la grille.

Le but est de pouvoir deviner correctement la position des atomes en utilisant le minimum de rayons possibles. 
Les rayons et les atomes peuvent interagir de façon suivante:

Toucher: Le rayon touche directement l’atome. Il ne ressort pas. 

Deflection: Un rayon ne peut pas passer directement a cote d’un atome. La position de l’atome est le centre d’une croix (haut, bas, gauche, droite) de 4 cases où les rayons peuvent pas passer à travers ni à côté.  
Chaque rayon qui passe à côté de ces cellules est dévié de 90 degrés.

Réflection: Le rayon revient à la même case depuis laquelle il a été envoyée. Cela peut se produire dans deux situations: 
	- il y a une case ‘interdite’, à côté de l’atome, directement sur la première case où le 	rayon est lancé. Il revient.
	- le rayon est dévié fois ou plus (deux atomes adjacents) ce qui le renvoie.



2. Features
	 	 	 		
A implémenter:
	- niveaux de difficulté
	- joueur vs pc
	- joueur vs joueur
	- rendu graphique
	- informations sur le joueur
	- fin de jeu (gagné / perdu)

Niveaux de difficulté: le joueur doit pouvoir choisir la taille de sa grille et le nombre d’atomes avant de commencer le jeu. Le joueur qui devine doit savoir la taille de la grille et le nombre d’atomes.

Joueur vs Ordinateur: l’ordinateur place les atomes au hasard sur la grille. L’humain doit deviner leur position.

Joueur vs Joueur en réseau: le premier joueur crée le jeu et place les atomes. L’autre joueur peut joindre le jeu en entrant l’adresse ip du premier joueur. Il va alors deviner.

Dans ce mode, le joueur qui a crée le jeu peut suivre les actions du joueur qui devine: il voit les rayons, les touches, les déflexions, …. L’animation et le rendu peuvent alors ce faire en 2D ou 3D. 



3. Rendu graphique

Le jeu doit inclure un rendu graphique en mode 1 et 2 joueurs. En mode 1 joueur l’interface doit: 
	- laisser le joueur envoyer les rayons depuis les bords du tableau
	- montrer et différencier toutes les entrées et sorties des rayons. Cela doit être visible. Des nombres, couleurs, etc peuvent être utilises
	- si le joueur fait un toucher ou une réflection, cela doit être indiqué

Dans un le mode 1 joueur, l’interface utilisateur ne doit jamais montrer le contenu réel de la grille.

Dans le mode player vs player, l’interface montre la grille cote  joueur qui place les atomes.  Il peut voir en ‘live’ les actions que fait le second joueur. 

Deviner - gagner / perdre 

Le joueur qui devine peut placer un atome à la position qu’il souhaite. Cela peut se faire à l’aide d’un clic, en entrant les coordonnées du tableau, etc…)

Le joueur ne peut pas essayer sauvagement toutes les positions possibles. S’il a quatre atomes, il peut entrer 4 coordonnées différentes. A chaque fois qu’il arrive a deviner la postions de l’atome, celui ci est révélé.

À la fin du jeu, quand le joueur a épuisé toutes les tentatives, le tableau est révélé et il peut voir la position des atomes de départ.

S’il devine la position des tous les atomes il a gagne. Sinon, il perd.



4. Rendu final

Les étudiants devront inclure les éléments suivants dans leur rendu final: 
	- une archive zip avec le code source final. Le code source doit contenir tous les outils nécessaires au fonctionnement de celui-ci (bibliothèques, etc…) 
	- documentation:
		- technique, expliquant vos choix et/ou vos choix d’implémentation / détails sur les points suivants:
			- moteur graphique
			- réseau
			- choix des algorithmes, gestion de la deflection et de la reflection
	- manuel du jeu 

Le premier document est académique. Il faut s’adresser au lecteur comme à un professeur, pas un client. Le deuxième document (manuel du jeu) doit s’adresser au lecteur en tant que client. 
Ces documents peuvent être en Français ou en Anglais.



5. Notation

Le projet sera note de façon suivante, sur une échelle de 28/31:

Documentation (3 points)
	Documentation utilisateur (1 point)
	Documentation technique (2 points)

Moteur de jeu (7 points)
	L’ordinateur peut placer les atomes sur la grille (1 point)
	Le moteur de jeu peut simuler correctement les interactions entre les rayons/atomes (6 points)

Noyau (7 points)
	Le joueur peut lancer un rayon (1 point)
	Le joueur peut voir ou sort le rayon (2 points)
	Le joueur peut voir les évènements de toucher / réflection (2 points)
	Le joueur peut facilement distinguer les différents rayons (entrée/sortie) (2 points)

Interface (11 points)
	Le joueur peut créer un jeu en solo (1 point)
	Le joueur peut créer un jeu multijoueur (2 joueurs) (1 point)
	Le joueur peut choisir un niveau de difficulté (taille de la grille, nombre d’atomes) (1 point)
	Un joueur peut rejoindre un jeu multijoueur (2 joueurs) (1 point)
	Le joueur qui devine voit seulement les entrées/sorties des rayons mais non les atomes (1 point)
	Le joueur qui place les atomes peut voir en “live” les actions du joueur qui devine (3 points) 
	Rendu 3D (3 points)

Bonus (3 points) 

			
And now… 
KEEP CALM AND ASK YODA 
KEEP CALM AND EAT PIZZA
